@extends('main')

@section('welcome')
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Register</a>
            @endif
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Oleksandr Pidmogylnui
        </div>

        <div class="links">
            <a href="https://laracasts.com">Laracasts</a>
            <a href="https://worldsmi.com">News</a>
            <a href="https://www.linkedin.com/in/oleksandrpidmogylnyi/">LinkedIn</a>
            <a href="https://gitlab.com/alexandr.cctv/work_search.git">GitLab</a>
        </div>
    </div>
@endsection