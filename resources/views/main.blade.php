<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
@include('header')
<body>
<div class="flex-center position-ref full-height">
    @yield('welcome')
    @yield('unsubscribe')
</div>
</body>
@include('footer')
</html>
