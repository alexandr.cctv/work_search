@extends('main')
@section('unsubscribe')
    <div class="content">
        <div class="title m-b-md">
            You have successfully unsubscribe
        </div>
        <div class="title m-b-md">
            {{$email}}
        </div>
        <div class="links">
            <a href="https://laracasts.com">Laracasts</a>
            <a href="https://worldsmi.com">News</a>
            <a href="https://www.linkedin.com/in/oleksandrpidmogylnyi/">LinkedIn</a>
            <a href="https://gitlab.com/alexandr.cctv/work_search.git">GitLab</a>
        </div>
    </div>
@endsection