<div style="font-size: 14px">
    <p>
        Good day!<br>
        You received this email because placed open position "{{$employer->position_title}}" on site <a href="https://larajobs.com">LaraJobs</a>.<br>
        At the moment I am looking for a job.<br>
        I developed web service to automatically search job for me. <a href="https://gitlab.com/alexandr.cctv/work_search">Link GitLab</a><br>
        If you consider my CV I will be very thankful you.<br>
        But If you send me your offer I will be very happy work with you and I will hard work to reach goals your company.<br>
        <br>
        If this mail not intresting for you, please press this link <a href="{{route('unsubscribe', ['email' => $employer->email])}}">Unsubscribe</a> and you will not receive mail from me.<br>
        <br>
        Best regards,<br>
        Pidmogylnyi Oleksandr<br>
        phone: +380969460245<br>
        email: alexandr.cctv@gmail.com<br>
        skype: alexandrchesnui<br>
    </p>
</div>
