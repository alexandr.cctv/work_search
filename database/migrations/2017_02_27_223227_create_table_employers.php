<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employers', function ($table) {
            $table->increments('id')->unsingned();
            $table->string('email');
            $table->string('position_title')->nullable();
            $table->text('position_description')->nullable();
            $table->string('position_url')->nullable();
            $table->enum('send_status', ['sent', 'error', 'not_sent'])->defoult('not_sent');
            $table->boolean('unsubscribe')->defoult(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employers');
    }
}
