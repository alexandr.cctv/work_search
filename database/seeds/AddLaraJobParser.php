<?php

use Illuminate\Database\Seeder;
use App\Models\ParseUrl;
use Illuminate\Support\Facades\Log;

class AddLaraJobParser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newParser = new ParseUrl();
        $newParser->worker = 'LaraJob';
        $newParser->url = 'https://larajobs.com/feed';

        if (!$newParser->save()) {
            Log::error('Data no seeding - ' . self::class);
        }
    }
}
