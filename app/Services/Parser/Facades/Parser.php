<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.02.17
 * Time: 1:00
 */

namespace App\Services\Parser\Facades;

use Illuminate\Support\Facades\Facade;

class Parser extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'parser';
    }
}
