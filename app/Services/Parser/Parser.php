<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.02.17
 * Time: 0:59
 */
namespace App\Services\Parser;

use App\Services\Parser\Workers\LaraJob;

class Parser extends ParserAbstract
{

    public function start()
    {
        foreach ($this->parsers as $parser) {
            $worker = $this->_loadWorker($parser);
            $worker->run();
        }
    }

    /**
     * @param $domElement
     */
    public function createLaraJobEmployer($domElement)
    {
        $laraJobWorker = new LaraJob();

        $laraJobWorker->createEmployer($domElement);
    }
}