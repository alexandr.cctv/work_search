<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.02.17
 * Time: 11:16
 */

namespace App\Services\Parser\Workers;

use App\Models\Employer as EmployerModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class Employer
{

    /**
     * Employer constructor.
     */
    public function __construct()
    {
        $this->email = '';
        $this->position_title = '';
        $this->position_description = '';
        $this->position_url = '';
        $this->send_status = 'not_sent';
        $this->unsubscribe = false;
        $this->hash = '';
        $this->pub_date = '';
    }

    /**
     * @return bool
     */
    public function save()
    {
        if ($this->_hasDuplicates()) {
            Log::error(Employer::class . ' - not save item has duplicates!');
            return false;
        }

        $employer = new EmployerModel();

        $employer->email = $this->email;
        $employer->position_title = $this->position_title;
        $employer->position_description = $this->position_description;
        $employer->position_url = $this->position_url;
        $employer->send_status = $this->send_status;
        $employer->unsubscribe = $this->unsubscribe;
        $employer->hash = $this->hash;
        $employer->pub_date = $this->pub_date;

        if ($employer->save()) {
            return true;
        }

        Log::error(Employer::class . ' - not save!');

        return false;
    }

    /**
     * @return bool
     */
    private function _hasDuplicates()
    {
        $validator = Validator::make(
            [
                'hash' => $this->hash,
                'email' => $this->email
            ],
            [
                'hash' => 'required|unique:employers',
                'email' => 'required|unique:employers|email'
            ]
        );

        if ($validator->fails()) {
            return true;
        }

        return false;
    }
}