<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.02.17
 * Time: 1:19
 */
namespace App\Services\Parser\Workers;

use anlutro\cURL\cURL;
use Symfony\Component\DomCrawler\Crawler;

abstract class WorkerAbstract
{

    protected $loadContent;
    protected $loadContentCrawler;

    abstract public function run();

    /**
     * WorkerAbstract constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->loadContent = $this->loadContent($url);

        if (!$this->loadContent) {
            return false;
        }

        $this->_setCrawler();
    }

    /**
     * @param $url
     * @return bool|string
     */
    protected function loadContent($url)
    {
        $curl = new cURL();

        $response = $curl->get($url);

        if (!$this->_checkHttpStatus($response)) {
            return false;
        }

        return $response->body;
    }

    /**
     * @return bool
     */
    private function _setCrawler()
    {
        $this->loadContentCrawler = new Crawler(
            $this->loadContent
        );

        return true;
    }

    /**
     * @param $response
     * @return bool
     */
    private function _checkHttpStatus(&$response)
    {
        if ($response->statusCode === 200) {
            return true;
        }
        return false;
    }
}