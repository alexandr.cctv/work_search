<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.02.17
 * Time: 1:19
 */
namespace App\Services\Parser\Workers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class LaraJob extends WorkerAbstract
{

    private $items;

    public function run()
    {
        $this->_getItems();
        foreach ($this->items as $domElement) {
            $this->_createEmployer($domElement);
        }
    }

    /**
     * @return bool
     */
    private function _getItems()
    {
        $this->items = $this->loadContentCrawler->filter('channel > item');

        return true;
    }

    /**
     * @param $domElement
     */
    private function _createEmployer($domElement)
    {
        $newEmployer = new Employer();

        foreach ($domElement->childNodes as $childNode) {
            if ($childNode->nodeName === 'link') {
                $newEmployer->email = $this->_getEmail(
                    $childNode->textContent
                );
                $newEmployer->position_url = $childNode->textContent;
                $newEmployer->hash = md5($childNode->textContent);
            }

            if ($childNode->nodeName === 'title') {
                $newEmployer->position_title = $childNode->textContent;
            }

            if ($childNode->nodeName === 'pubDate') {
                $newEmployer->pub_date = $childNode->textContent;
            }

            if ($childNode->nodeName === 'description') {
                $newEmployer->position_description = $childNode->textContent;
            }
        }

        $newEmployer->save();
    }

    /**
     * @param $link
     * @return bool
     */
    private function _getEmail($link)
    {
        sleep(env('SLEEP_TIME'));

        $this->_makeJsFile($link);

        $phantom_script = dirname(__FILE__) . '/get-website.js';

        //$process = new Process('xvfb-run phantomjs ' . $phantom_script);
        $process = new Process('phantomjs ' . $phantom_script);

        $process->setWorkingDirectory(dirname(__FILE__));

        $process->start();

        $process->wait();

        $outString = $process->getOutput();

        echo $outString;

        $pattern = '/([-_.a-z0-9]+)@([-_a-z0-9.]+[-_a-z0-9])/is';

        preg_match($pattern, $outString, $matches);

        foreach ($matches as $value) {
            $validator = Validator::make(
                [
                    'email' => $value
                ],
                [
                    'email' => 'required|email'
                ]
            );
            if (!$validator->fails()) {
                return $value;
            }
        }
        return false;
    }

    /**
     * @param $url
     */
    private function _makeJsFile($url)
    {
        $fileContent = "
        var webPage = require('webpage');
        var page = webPage.create();
        page.open('$url', function(status) {
            console.log(page.content);
            phantom.exit();
        });";

        Storage::disk('parser')->put('get-website.js', $fileContent);
    }


}