<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.02.17
 * Time: 1:01
 */
namespace App\Services\Parser;

use App\Models\ParseUrl;
use Illuminate\Support\Facades\Log;
use ReflectionClass;

abstract class ParserAbstract
{

    protected $parsers;
    protected $workerNamespace;

    /**
     * ParserAbstract constructor.
     */
    public function __construct()
    {
        $this->parsers = ParseUrl::all();
    }

    /**
     * @param $worker
     * @return bool
     */
    protected function _loadWorker($worker)
    {
        $this->setUpNamespace();

        if (!class_exists($this->workerNamespace . $worker->worker)) {
            Log::error(
                "Class ($this->workerNamespace $worker->worker) Not Found"
            );
            return false;
        }

        $class = $this->workerNamespace . $worker->worker;

        return new $class($worker->url);
    }

    /**
     * @return bool
     */
    private function setUpNamespace()
    {
        $className = get_class($this);

        $reflection = new ReflectionClass($className);

        $namespace = $reflection->getNamespaceName();

        $this->workerNamespace = "\\$namespace\\Workers\\";

        return true;
    }
}