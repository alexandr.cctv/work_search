<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 02.03.17
 * Time: 0:05
 */

namespace App\Services\Sender\Facades;

use Illuminate\Support\Facades\Facade;

class Sender extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sender';
    }
}