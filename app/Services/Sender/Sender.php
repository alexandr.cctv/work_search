<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 02.03.17
 * Time: 0:03
 */

namespace App\Services\Sender;

use App\Models\Employer;
use Illuminate\Support\Facades\Mail;

class Sender extends SenderAbstract
{

    /**
     * @return bool
     */
    public function sendCV()
    {
        $employer = $this->_getNotSentEmployer();

        if (!$employer) {
            return false;
        }

        Mail::send(
            'emails.cv',
            ['employer' => $employer],
            function ($message) use ($employer) {
                $message->from(env('MAIL_USERNAME'), 'Full Stack Web Developer');
                $message->to('alexandr.cctv@gmail.com', 'Employer');
                //$message->to($employer->email, 'Employer');
                $message->subject($employer->position_title);
                $message->attach(storage_path('app/public') . '/cv.pdf');
            });

        $employer->send_status = 'sent';
        return $employer->save();
    }

    /**
     * @return mixed
     */
    private function _getNotSentEmployer()
    {
        $employer = Employer::where(
            'send_status',
            'not_sent'
        )->where(
            'unsubscribe',
            false
        )->first();

        return $employer;
    }
}