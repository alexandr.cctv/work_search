<?php

namespace App\Providers;

use App\Services\Parser\Parser;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class ParserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('parser', function () {
            return new Parser();
        });
    }
}
