<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParseUrl extends Model
{
    public $table = 'parse_urls';
}
