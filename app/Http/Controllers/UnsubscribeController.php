<?php

namespace App\Http\Controllers;

use App\Models\Employer;

class UnsubscribeController extends Controller
{
    /**
     * @param $email
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index($email)
    {
        $employer = Employer::where(
            'email',
            $email
        )->first();

        if (!$employer) {
            return abort(404);
        }

        $employer->unsubscribe = true;

        if ($employer->save()) {
            return view('unsubscribe_complite', ['email' => $employer->email]);
        }
        return abort(404);
    }
}
